#ifndef _visualSTATE_SYSTITILASEMLIBB_H
#define _visualSTATE_SYSTITILASEMLIBB_H

/*
 * Id:        SysTitilaSEMLibB.h
 *
 * Function:  Contains definitions needed for API functions.
 *
 * Generated: Sun May 17 11:53:20 2015
 *
 * Coder 6, 4, 1, 1980
 * 
 * This is an automatically generated file. It will be overwritten by the Coder.
 * 
 * DO NOT EDIT THE FILE!
 */


#include "SysTitilaSEMBDef.h"


#if (VS_CODER_GUID != 0X0035913c8L)
#error The generated file does not match the SEMTypes.h header file.
#endif


#ifndef VS_TRUE
#define VS_TRUE (1)
#endif


#ifndef VS_FALSE
#define VS_FALSE (0)
#endif


/*
 * Extern variable declarations
 */
extern VS_INT vContador;


/*
 * Event Identifier Definitions.
 */
#define SE_RESET 0u
#define evNoPulsado 1u
#define evPulsado 2u
#define evTimer 3u



#ifndef VS_COMPLETION_CODES_DEFINED
#define VS_COMPLETION_CODES_DEFINED
enum
{
  /*
   * Status code:     SES_OKAY.
   *
   * Explanation:     Function performed successfully.
   *
   * Solution:        None.
   */
  SES_OKAY, /* 0 */


  /*
   * Status code:     SES_FOUND.
   *
   * Explanation:     The called function has returned an identifier index number.
   *
   * Solution:        Process the returned identifier index number. If the
   *                  function SEM_GetInput or SEM_GetOutput was called, the
   *                  function can be called again to find more events or
   *                  action expressions.
   */
  SES_FOUND, /* 1 */


  /*
   * Status code:     SES_ACTIVE.
   *
   * Explanation:     The completion code has one of the two expositions:
   *                  1)  A state/event deduction is started, while an event
   *                      inquiry is active. All inquired events have not been
   *                      returned by the function SEM_GetInput.
   *                  2)  An event inquiry is started, while a state/event
   *                      deduction is active. All deduced action expressions 
   *                      have not been returned by the function SEM_GetOutput 
   *                      and the SEM_NextState has not been called in order to 
   *                      complete the state/event deduction.
   *
   * Solution:        The completion code is a warning and perhaps the
   *                  application program should be rewritten. An event inquiry
   *                  and a state/event deduction should not be active at the
   *                  same time.
   */
  SES_ACTIVE, /* 2 */


  /*
   * Error code:      SES_CONTRADICTION.
   *
   * Explanation:     A contradiction has been detected between two states in a
   *                  state machine.
   *
   * Solution:        Check the VS System.
   */
  SES_CONTRADICTION, /* 3 */


  /*
   * Error code:      SES_RANGE_ERR.
   *
   * Explanation:     You are making a reference to an identifier that does not
   *                  exist. Note that the first index number is 0. If the 
   *                  VS System has 4 identifiers of the same type and you use a
   *                  function with the variable parameter = 4, the function 
   *                  will return an SES_RANGE_ERR error. In this case the 
   *                  highest variable index number is 3.
   *
   * Solution:        The application program will check the index parameters 
   *                  with one of the following symbols defined in the SEMBDef.h 
   *                  file:
   *
   *                    VS_NOF_EVENTS
   *                    VS_NOF_STATES
   *                    VS_NOF_ACTION_FUNCTIONS
   *                    VS_NOF_STATE_MACHINES
   */
  SES_RANGE_ERR, /* 4 */


  /*
   * Error code:      SES_TEXT_TOO_LONG.
   *
   * Explanation:     The requested text is longer than the specified maximum length.
   *
   * Solution:        Increase the maximum length.
   */
  SES_TEXT_TOO_LONG, /* 5 */


   /*
   * Error code:      SES_TYPE_ERR.
   *
   * Explanation:     A text function has been called with the wrong identifier
   *                  type or the specified text is not included in the VS System.
   *
   * Solution:        Use the identifier type symbols (EVENT_TYPE, STATE_TYPE
   *                  or ACTION_TYPE) defined in this file and remember
   *                  to include wanted text in the VS System.
   */
  SES_TYPE_ERR, /* 6 */


  /*
   * Error code:      SES_EMPTY.
   *
   * Explanation:     No events have been given to the SEM_Deduct function before
   *                  calling this function.
   *
   * Solution:        Call the SEM_Deduct function with an event number.
   */
  SES_EMPTY, /* 7 */


  /*
   * Error code:      SES_BUFFER_OVERFLOW.
   *
   * Explanation:     A destination buffer cannot hold the number of items found.
   *
   * Solution:        Call the function with an extended buffer as destination.
   */
  SES_BUFFER_OVERFLOW, /* 8 */


  /*
   * Error code:      SES_SIGNAL_QUEUE_FULL.
   *
   * Explanation:     Signal queue is full.
   *
   * Solution:        Increase the maximum signal queue size in the VS System or
   *                  via the VS Coder signal queue size option.
   */
  SES_SIGNAL_QUEUE_FULL, /* 9 */


  /*
   * Error code:      SES_NOT_INITIALIZED.
   *
   * Explanation:     The system has not been initialized.
   *
   * Solution:        Call the initialization function for the VS System.
   */
  SES_NOT_INITIALIZED /* 10 */
};
#endif /* VS_COMPLETION_CODES_DEFINED */


/* Identifier types, used when getting texts and explanations.*/
#ifndef VS_IDENTIFIER_TYPES_DEFINED
#define VS_IDENTIFIER_TYPES_DEFINED
enum {EVENT_TYPE, STATE_TYPE};
#endif /* VS_IDENTIFIER_TYPES_DEFINED */


/*
 * Name        : SysTitilaVSDeduct
 *
 * Description : The function deduces all the relevant action expressions on
 *               the basis of the given event, the internal current state
 *               vector and the transitions in the VS System. All the
 *               relevant action expressions are then called and all the
 *               next states are changed.
 *
 * Argument    : EventNo:
 *                 Event number to be processed.
 *
 *               Variable number of arguments:
 *                 Used if at least one event has a parameter.
 *                 The function call must include one argument for each type
 *                 name declared in the parameter list for each event.
 *                 
 *                 This sample declaration is for an event with three
 *                 parameters:
 *                 
 *                   EventName (VS_UINT8 par1, VS_CHAR par2, VS_INT par3)
 *                 
 *                 How to call the SysTitilaVSDeduct function for the event
 *                 EventName:
 *                 
 *                   SysTitilaVSDeduct(EventName, par1, par2, par3);
 *
 * Return      : Completion code:
 *
 *                 SES_CONTRADICTION:
 *                   Contradiction detected, the VS System is not
 *                   consistent. Check the VS System.
 *                   You will also get this error code here if you forget to
 *                   call the SysTitilaVSInitAll function.
 *
 *                 SES_SIGNAL_QUEUE_FULL:
 *                   The signal queue is full. Increase the signal queue
 *                   size in the VS System.
 *
 *                 SES_FOUND:
 *                   Success. The internal state vector was updated.
 *
 *                 SES_RANGE_ERR:
 *                   The event given to this function is out of range.
 *
 *                 SES_OKAY:
 *                   Success.
 *
 * Portability : ANSI-C Compiler.
 */
VS_UINT8 System1VSDeduct  (SEM_EVENT_TYPE EventNo);


/*
 * Name        : SysTitilaVSInitAll
 *
 * Description : The function is a wrapper to all initialization 
 *               functions. The function calls the following functions
 *               in the listed order (provided the specific function 
 *               exists): 
 *                 SysTitilaVSInitExternalVariables
 *                 SysTitilaVSInitInternalVariables
 *
 * Argument    : None.
 */
void SysTitilaVSInitAll (void);


/*
 * Action Function Prototypes
 */
extern VS_VOID AcApagar (VS_VOID);
extern VS_VOID AcPrender (VS_VOID);


#endif /* ifndef _visualSTATE_SYSTITILASEMLIBB_H */
