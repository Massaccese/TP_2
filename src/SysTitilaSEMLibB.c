/*
 * Id:        SysTitilaSEMLibB.c
 *
 * Function:  Contains all API functions.
 *
 * Generated: Sun May 17 11:53:20 2015
 *
 * Coder 6, 4, 1, 1980
 * 
 * This is an automatically generated file. It will be overwritten by the Coder.
 * 
 * DO NOT EDIT THE FILE!
 */


#include "SysTitilaSEMLibB.h"


#if (VS_CODER_GUID != 0X0035913c8L)
#error The generated file does not match the SEMTypes.h header file.
#endif


#define ESTADO SysTitilaCSV


/*
 * State Identifier Definitions.
 */
#define REPOSO 0u
#define RETARDO_TITILA 1u
#define RETARDO_REPOSO 2u
#define PRENDIDO 3u
#define APAGADO 4u
#define CANT_LED_PRENDIDO 5u
#define CANT_LED_APAGADO  5u

static SEM_STATE_TYPE SysTitilaCSV[VS_NOF_STATE_MACHINES];

VS_INT vCONTADOR  = 0;
VS_INT vCONT_LedPrendido  = 0;
VS_INT vCONT_LedApagado   = 0;

/**
 *  @brief Esta función tiene la finalidad de recibir el evento los cuales pueden ser
 *  evPulsado, evNopulsado,  evtimer o SE_RESET y reaccionar en consecuencia obteniendo un estado entre los que se encuentran los siguientes
 *  0- REPOSO			: Este estado es el inicial
 *  1- RETARDO_TITILA	: Es un estado intermedio que tiene la finalidad de evitar el rebote al presionar el pulsador antes que el led titile en forma onstante
 *  2- RETARDO_REPOSO	: Es un estado intermedio que tiene la finalidad de evitar el rebote al soltar el pulsador antes que la maquina de estado que en REPOSO.
 *  3- PRENDIDO			: ES el estado que se intercambia con APAGADO logrando el efecto de titilación.
 *  4- APAGADO			: ES el estado que se intercambia con PRENDIDO logrando el efecto de titilación.
 *
 *  @param EventNo corresponde al evento que se produce
 *  1- si el evento es  SE_RESET el estado obtenido será el REPOSO.
 *  2- si el evento es  evNoPulsado, el resultado depende del estado iniciaL:
 *  		2.1- si el estado inicial es RETARDO_TITILA, migrará al estado PRINDIDO que se intercambiará con el estado APAGADO.
 *  		2.2- si el estado inicial es RETARDO_REPOSO, migrará al estado REPOSO.
 *  3- si el evento es  evPulsado, el resultado depende del estado iniciaL:
 *		  	3.1- si el estado inicial es REPOSO, migrará al estado RETARDO_TITILA
 *		  	3.2- si el estado inicial es PRENDIDO o APAGADO, migrará al estado RETARDO_REPOSO
 *  4- si el evento es evTimer, el resultado depende del estado iniciaL:
 *		  	4.1- si el estado inicial es RETARDO_TITILA o RETARDO_REPOSO, disminuirá un contador interno hasta que se estabilice el rebote del pulsador
 *		  		ya sea al presionarlo como al soltarlo.
 *		  	4.2- si el estado inicial es PRENDIDO, esperará un tiempo y luego pasará al estado APAGADO
 *		  	4.3- si el estado inicial es APAGADO, esperará un tiempo y luego pasará al estado PRENDIDO
 *
 *	@return un valor de tipo VS_UINT8 que corresponde a un codigo de error u okay seguSystem1VSDeduct n corresponda
 */

VS_UINT8 System1VSDeduct  (SEM_EVENT_TYPE EventNo)
{
/* scoped for events */
{
  SEM_STATE_TYPE WSV[VS_NOF_STATE_MACHINES] =
  {
    STATE_UNDEFINED
  };

  switch (EventNo)
  {
  case SE_RESET:
    {
      acApagar();
      WSV[0] = REPOSO;
    }
    break;

  case evNoPulsado:
    {
        if ((ESTADO[0] == RETARDO_TITILA)
          && (vCONTADOR == 0))
        {
          acPrender();
      	vCONT_LedPrendido = CANT_LED_PRENDIDO;
          WSV[0] = PRENDIDO;
        }

        if ((ESTADO[0] == RETARDO_REPOSO)
          && (vCONTADOR == 0))
        {
          acApagar();
          WSV[0] = REPOSO;
        }
    }
    break;

  case evPulsado:
    {
      if ((ESTADO[0] == REPOSO))
      {
        vCONTADOR = 20;
        WSV[0] = RETARDO_TITILA;
      }

      if ((ESTADO[0] == PRENDIDO || ESTADO[0] == APAGADO))
      {
        vCONTADOR = 20;
        WSV[0] = RETARDO_REPOSO;
      }
    }
    break;

  case evTimer:
    {
      if ((ESTADO[0] == RETARDO_TITILA))
      {
        vCONTADOR = vCONTADOR - 1;
        WSV[0] = RETARDO_TITILA;
      }
      if ((ESTADO[0] == RETARDO_REPOSO))
      {
        vCONTADOR = vCONTADOR - 1;
        WSV[0] = RETARDO_REPOSO;
      }

      if (ESTADO[0] == PRENDIDO)
      {
    	  if(vCONT_LedPrendido>0)
    	  {
    		  vCONT_LedPrendido = vCONT_LedPrendido -1;
    	  } else {
				acApagar();
				WSV[0] = APAGADO;
				vCONT_LedApagado = CANT_LED_APAGADO;
    	  }
      }
      if (ESTADO[0] == APAGADO)
      {
    	  if (vCONT_LedApagado>0)
    	  {
    		  vCONT_LedApagado = vCONT_LedApagado -1;
    	  } else {
				acPrender();
				WSV[0] = PRENDIDO;
				vCONT_LedPrendido = CANT_LED_PRENDIDO;
    	  }
    }
    break;
    }
  default:
    return SES_RANGE_ERR;
  }

  //Este ciclo tiene la finlidad de pasar es estado local que se guarda en la variable WSV a la variable ESTADO, que es global
  {
    VS_UINT i;
    for (i = 0u; i < VS_NOF_STATE_MACHINES; ++i)
    {
      if (WSV[i] != STATE_UNDEFINED)
      {
        ESTADO[i] = WSV[i];
      }
    }
  }
}

return SES_OKAY;
}


void SysTitilaVSInitAll (void)
{
}
