/* Cop9yright 2015, Claudio Acuña, Gabriel Casas and Bruno Massaccese.
 *
 * This file is part of TD2-Template.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief This is a simple C example file.
 **
 **/

/** \addtogroup TD2 Técnicas Digitales II
 ** @{ */

/** @addtogroup App Aplicación de usuario
 * 	@{
 */

/*
 * Initials     Name
 * ---------------------------
 * BM           Bruno Massaccese
 * GC			Gabriel Casas
 * CA			Claudio Acuña
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20150517 v0.0.1   CA, GC, BR first version
 */

/*==================[inclusions]=============================================*/

#include <cr_section_macros.h>
#include "chip.h"
#include "board.h"
#include "SysTitilaSEMLibB.h"

/*==================[macros and definitions]=================================*/

#define TICKRATE_HZ1 (10)

/*==================[internal functions declaration]=========================*/

/*==================[internal data declaration]==============================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/**
 * @brief esta función maneja el Systick
 * @return None
 */
void SysTick_Handler(void){
	System1VSDeduct(evTimer);
}

int main(void)
{
	bool PORT=0;
    // Read clock settings and update SystemCoreClock variable
    SystemCoreClockUpdate();
    // functions related to the board hardware
    Board_Init();

    // los parametros son :
    // 1- un puntero a iocon
    // 2- el puerto
    // 3- el pin
    // 4- el  modo 00 corresponde a "Pin has an on-chip pull-up resistor enabled"
    // 5- la funcion 00 corresponde a "Pin is in the normal (not open drain) mode."

    //esta funcion hace lo mismo que la segunda, con la diferencia que acá utiliza constantes definidas
     Chip_IOCON_PinMux(LPC_IOCON,0,18, IOCON_MODE_PULLUP,IOCON_FUNC0);

    // Esta funcion internamente llama a Chip_GPIO_SetPinDIRInput
    Chip_GPIO_SetPinDIR(LPC_GPIO,0,18,false);

    // Set the LED to the state of "On"

    Board_LED_Set(0, false);

    SysTick_Config(SystemCoreClock / TICKRATE_HZ1);
    System1VSDeduct(SE_RESET);
    // Este siclo corre infinifamente y reconoce si se pulso o no el pulsador

    while(1) {
      PORT=Chip_GPIO_ReadPortBit(LPC_GPIO,0,18); /* LEE EL PIN 18 DEL PUERTO 0 */

      //Para este caso el valor de port está negado debido a que estamos trabajando con una resistencia en pull up
      //con este tipo de configuración simpre la entrada está en 1
    	if(!PORT){						 /*PREGUNTO SI SE PULSO EL BOTON */
    		System1VSDeduct(evPulsado);  /*SE ENVIA A LA MAQUINA DE ESTADOS EL EVENTO PULSADO*/
       }
    	else
    		System1VSDeduct(evNoPulsado);
    }
    return 0 ;
}


/** @brief esta función prende el led
 *	@return none
 */
void acPrender(void)
{

	Chip_GPIO_WritePortBit(LPC_GPIO,0,22,1); /*ESCRIBO UN 1 EN EL PUERTO DEL LED */

}

/** @brief esta función apaga el led
 *	@return none
 */
void acApagar(void)
{

	Chip_GPIO_WritePortBit(LPC_GPIO,0,22,0); /*ESCRIBO UN 0 EN EL PUERTO DEL LED */

}

/*==================[external functions definition]==========================*/


/** @} doxygen end group definition */
/** @} doxygen end group definition */

/*==================[end of file]============================================*/

